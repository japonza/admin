module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
    url: "https://japonza.ru/api",
  admin: {
     

    url: 'https://japonza.ru/dashboard',

   auth: {
      secret: env('ADMIN_JWT_SECRET', '444ecf803d984742d0d0f793ed708697'),
    },
  },
});
