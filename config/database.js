module.exports = ({ env }) => ({
  defaultConnection: 'default',
  connections: {
    default: {
      connector: 'bookshelf',
      settings: {
        client: 'postgres',
        host: env('DATABASE_HOST', 'localhost'),
        port: env.int('DATABASE_PORT', 5432),
        database: env('DATABASE_NAME', 'japonza'),
        username: env('DATABASE_USERNAME', 'japonzauser'),
        password: env('DATABASE_PASSWORD', '6465larodo'),
      },
      options: {
        useNullAsDefault: true,
      },
    },
  },
});
